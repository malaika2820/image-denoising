#importing libaries
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import random 

#loading data-set
(X_train, y_train), (X_test, y_test) = tf.keras.datasets.fashion_mnist.load_data()

#adding noise to data
X_train = X_train / 255
X_test = X_test / 255

noise_factor = 0.3

noise_dataset = []

for img in X_train:
  noisy_image = img + noise_factor * np.random.randn(*img.shape)
  noisy_image = np.clip(noisy_image, 0., 1.)
  noise_dataset.append(noisy_image)

noise_test_dataset = []

for img in X_test:
  noisy_image = img + noise_factor * np.random.randn(*img.shape)
  noisy_image = np.clip(noisy_image, 0., 1.)
  noise_test_dataset.append(noisy_image)

noise_dataset = np.array(noise_dataset)
noise_test_dataset = np.array(noise_test_dataset)

