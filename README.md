# Image Denoising

Denoising images using AutoEncoders in Python

The AutoEncoder trains on 60,000 data points of the fashion data-set, and 10,000 data points are used for testing. 

**Test Loss** : 0.303

**Sample Noisy/Denoised image** -

![download.png](images/download.png)